<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<?php get_sidebar('shop-left'); ?>

						<div id="main" class="sixcol clearfix" role="main">

							<?php woocommerce_breadcrumb(); ?>
							
							<?php woocommerce_content(); ?>

						</div> <!-- end #main -->

						<?php get_sidebar('shop-right'); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
