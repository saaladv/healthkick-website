<?php get_header(); 

	global $wp_query, $woocommerce;
?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix woocommerce">

					<div id="main" class="twelvecol first clearfix" role="main">

						<article id="post-not-found" class="hentry clearfix">

							<header class="article-header">

								<h1><?php _e("Oh Snap....this page is missing!", "bonestheme"); ?></h1>

							</header> <!-- end article header -->

							<section class="entry-content clearfix">

								<p><?php _e("Unfortunately, the page you are trying to access cannot be found.  Please give it another go, and if you are still seeing this message, the page may have been moved or deleted.", "bonestheme"); ?></p>

								<p><?php _e("Try to find what you are looking for below &darr;", "bonestheme"); ?></p>
							</section>
							<section class="clearfix">
									
								<section class="threecol first sidebar clearfix">
									<?php the_widget('WC_Widget_Product_Search', array(
										'title'			=> 'Search for Products',
									)); ?>
									<?php the_widget('WC_Widget_Product_Categories', array(
										'title'			=> 'Shop by Category',
										'orderby'		=> 'name',
										'count'			=>  0,
										'hierarchical'	=> 	true,
										'dropdown'		=>  1
									)); ?>
									<div class="widget woocommerce">
										<h2 class="widgettitle">Shop by Brand</h2>
										<a href="<?php echo get_page_link(13);?>" class="button">View list of Brands</a>
									</div>
								</section>
								<section class="threecol sidebar clearfix">
									<?php the_widget('WC_Widget_Products', array(
										'title'			=> 'Sale Products',
										'number'		=> 4,
										'show'			=> '',
										'orderby'		=> 'name',
										'order'			=> 'rand'
									)); ?> 
								</section>
								<section class="threecol sidebar clearfix">
									<?php the_widget('WC_Widget_Recently_Viewed', array(
										'title'			=> 'Recently Viewed Products',
										'orderby'		=> 'name',
										'number'		=> 4
									)); ?> 
									
								</section>
								<section class="threecol last sidebar clearfix">
									 <?php the_widget('WC_Widget_Top_Rated_Products', array(
										'title'			=> 'Top Rated Products',
										'orderby'		=> 'name',
										'number'		=> 4
									)); ?> 
								</section>
								

							</section> <!-- end article section -->

						</article> <!-- end article -->

					</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
