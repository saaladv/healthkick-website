/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'healthkick\'">' + entity + '</span>' + html;
	}
	var icons = {
			'ic-cart' : '&#x21;',
			'ic-arrow-right-alt1' : '&#x22;',
			'ic-arrow-right' : '&#x23;',
			'ic-price' : '&#x24;',
			'ic-circleadd' : '&#x27;',
			'ic-circledelete' : '&#x28;',
			'ic-circleselect' : '&#x29;',
			'ic-alarm' : '&#x2a;',
			'ic-fire' : '&#x2b;',
			'ic-plus' : '&#x2c;',
			'ic-minus' : '&#x2d;',
			'ic-comment' : '&#x2e;',
			'ic-user' : '&#x2f;',
			'ic-users' : '&#x30;',
			'ic-arrow-left' : '&#x31;',
			'ic-arrow-down' : '&#x32;',
			'ic-arrow-up' : '&#x33;',
			'ic-credit' : '&#x34;',
			'ic-user-2' : '&#x35;',
			'ic-users-2' : '&#x36;',
			'ic-stats' : '&#x37;',
			'ic-signup' : '&#x38;',
			'ic-star' : '&#x39;',
			'ic-star-2' : '&#x3a;',
			'ic-star-3' : '&#x3b;',
			'ic-heart' : '&#x3c;',
			'ic-heart-2' : '&#x3d;',
			'ic-info' : '&#x3e;',
			'ic-info-2' : '&#x3f;',
			'ic-facebook' : '&#x40;',
			'ic-twitter' : '&#x41;',
			'ic-linkedin' : '&#x42;',
			'ic-pinterest' : '&#x43;',
			'ic-certificate' : '&#x44;',
			'ic-email' : '&#x46;',
			'ic-uniF013' : '&#x47;',
			'ic-search' : '&#x48;',
			'ic-tag' : '&#x25;',
			'ic-profile' : '&#x26;',
			'ic-t-shirt' : '&#x45;',
			'ic-menu' : '&#x49;',
			'ic-list' : '&#x4a;',
			'ic-list-2' : '&#x4b;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/ic-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};