<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'saal_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function saal_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_saal_';

	// Staff Page Info Box
	$meta_boxes[] = array(
		'id'         => 'staff_info',
		'title'      => 'Staff Information',
		'pages'      => array( 'staff', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Position',
				'desc' => 'Staff member position',
				'id'   => $prefix . 'position',
				'type' => 'text',
			),
			array(
				'name' => 'Email',
				'desc' => 'Contact email address',
				'id'   => $prefix . 'staff_email',
				'type' => 'text',
			),
			array(
				'name'     => 'HKN Location',
				'desc'     => 'Location this employee works at',
				'id'       => $prefix . 'staff_location',
				'type'     => 'cpt_select',
				'posttype' => 'location', // Taxonomy Slug
			),
		),
	);

	// All Page Boxes
	$meta_boxes[] = array(
		'id'         => 'page_settings',
		'title'      => 'Page Settings',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Display Page Title',
				'desc' => 'Show/Hide Page title',
				'id' => $prefix . 'show_page_title',
				'type' => 'radio_inline',
				'options' => array(
					array('name' => 'Show', 'value' => '1'),
					array('name' => 'Hide', 'value' => '2'),			
				),
				'std' => '1'
			),
		),
	);

	// Deal of the Week Metaboxes
	$meta_boxes[] = array(
		'id'         => 'deal_details',
		'title'      => 'Deal Details',
		'pages'      => array( 'deal', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Deal Start',
				'desc' => 'Select the date you would like this deal to start.',
				'id' => $prefix . 'deal_start_date',
				'type' => 'text_date_timestamp',
			),
			array(
				'name' => 'Deal Expiration',
				'desc' => 'Select the date you would like this deal to expire.',
				'id' => $prefix . 'deal_exp_date',
				'type' => 'text_date_timestamp',
			),
			array(
				'name' => 'Offer Text',
				'desc' => 'Offer text (ex: 25% OFF)',
				'id' => $prefix . 'deal_offer',
				'type' => 'text',
			),
			array(
				'name' => 'Offer Sub-Text',
				'desc' => 'Offer sub-text (ex: Twinlab Workout Pack)',
				'id' => $prefix . 'deal_subtext',
				'type' => 'text',
			),
			array(
				'name' => 'Offer Link',
				'desc' => 'Link that user will be directed to when they click on the offer',
				'id' => $prefix . 'deal_link',
				'type' => 'text',
			),
			array(
				'name' => 'Offer Image',
				'desc' => 'Background image for the offer',
				'id' => $prefix . 'deal_image',
				'type' => 'file',
				'save_id' => true, // save ID using true
				'allow' => array( 'attachment' )
			),
		),
	);

	//Location Page Info Box
	$meta_boxes[] = array(
		'id'         => 'location_info',
		'title'      => 'Location Information',
		'pages'      => array( 'location', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Address',
				'desc' => 'Location Address',
				'id'   => $prefix . 'location_address',
				'type' => 'text',
			),
			array(
				'name' => 'City',
				'desc' => 'Location City',
				'id'   => $prefix . 'location_city',
				'type' => 'text',
			),
			array(
				'name' => 'State',
				'desc' => 'Location State',
				'id'   => $prefix . 'location_state',
				'type' => 'select',
				'options' => array (
						'' => array(
							'name' => '--',
							'value' => ''
							),
						'AL' => array(
							'name' => 'Alabama',
							'value' => 'AL'
							),
						'AK' => array(
							'name' => 'Alaska',
							'value' => 'AK'
							),
						'AZ' => array(
							'name' => 'Arazona',
							'value' => 'AZ'
							),
						'AR' => array(
							'name' => 'Arkansas',
							'value' => 'AR'
							),
						'CA' => array(
							'name' => 'California',
							'value' => 'CA'
							),
						'CO' => array(
							'name' => 'Colorado',
							'value' => 'CO'
							),
						'CT' => array(
							'name' => 'Connecticut',
							'value' => 'CT'
							),
						'DE' => array(
							'name' => 'Delaware',
							'value' => 'DE'
							),
						'DC' => array(
							'name' => 'District of Columbia',
							'value' => 'DC'
							),
						'FL' => array(
							'name' => 'Florida',
							'value' => 'FL'
							),
						'GA' => array(
							'name' => 'Georgia',
							'value' => 'GA'
							),
						'HI' => array(
							'name' => 'Hawaii',
							'value' => 'HI'
							),
						'ID' => array(
							'name' => 'Idaho',
							'value' => 'ID'
							),
						'IL' => array(
							'name' => 'Illinois',
							'value' => 'IL'
							),
						'IN' => array(
							'name' => 'Indiana',
							'value' => 'IN'
							),
						'IA' => array(
							'name' => 'Iowa',
							'value' => 'IA'
							),
						'KS' => array(
							'name' => 'Kansas',
							'value' => 'KS'
							),
						'KY' => array(
							'name' => 'Kentucky',
							'value' => 'KY'
							),
						'LA' => array(
							'name' => 'Louisiana',
							'value' => 'LA'
							),
						'ME' => array(
							'name' => 'Maine',
							'value' => 'ME'
							),
						'MD' => array(
							'name' => 'Maryland',
							'value' => 'MD'
							),
						'MA' => array(
							'name' => 'Massachusetts',
							'value' => 'MA'
							),
						'MI' => array(
							'name' => 'Michigan',
							'value' => 'MI'
							),
						'MN' => array(
							'name' => 'Minnesota',
							'value' => 'MN'
							),
						'MS' => array(
							'name' => 'Mississippi',
							'value' => 'MS'
							),
						'MO' => array(
							'name' => 'Missouri',
							'value' => 'MO'
							),
						'MT' => array(
							'name' => 'Montana',
							'value' => 'MT'
							),
						'NE' => array(
							'name' => 'Nebraska',
							'value' => 'NE'
							),
						'NV' => array(
							'name' => 'Nevada',
							'value' => 'NV'
							),
						'NH' => array(
							'name' => 'New Hampshire',
							'value' => 'NH'
							),
						'NJ' => array(
							'name' => 'New Jersey',
							'value' => 'NJ'
							),
						'NM' => array(
							'name' => 'New Mexico',
							'value' => 'NM'
							),
						'NY' => array(
							'name' => 'New York',
							'value' => 'NY'
							),
						'NC' => array(
							'name' => 'North Carolina',
							'value' => 'NC'
							),
						'ND' => array(
							'name' => 'North Dakota',
							'value' => 'ND'
							),
						'OH' => array(
							'name' => 'Ohio',
							'value' => 'OH'
							),
						'OK' => array(
							'name' => 'Oklahoma',
							'value' => 'OK'
							),
						'OR' => array(
							'name' => 'Oregon',
							'value' => 'OR'
							),
						'PA' => array(
							'name' => 'Pennsylvania',
							'value' => 'PA'
							),
						'RI' => array(
							'name' => 'Rhode Island',
							'value' => 'RI'
							),
						'SC' => array(
							'name' => 'South Carolina',
							'value' => 'SC'
							),
						'SD' => array(
							'name' => 'South Dakota',
							'value' => 'SD'
							),
						'TN' => array(
							'name' => 'Tennessee',
							'value' => 'TN'
							),
						'TX' => array(
							'name' => 'Texas',
							'value' => 'TX'
							),
						'UT' => array(
							'name' => 'Utah',
							'value' => 'UT'
							),
						'VT' => array(
							'name' => 'Vermont',
							'value' => 'VT'
							),
						'VA' => array(
							'name' => 'Verginia',
							'value' => 'VA'
							),
						'WA' => array(
							'name' => 'Washington',
							'value' => 'WA'
							),
						'WV' => array(
							'name' => 'West Virginia',
							'value' => 'WV'
							),
						'WI' => array(
							'name' => 'Wisconsin',
							'value' => 'WI'
							),
						'WY' => array(
							'name' => 'Wyoming',
							'value' => 'WY'
							)
						)				
			),
			array(
				'name' => 'Zip',
				'desc' => 'Zip Code for this location',
				'id'   => $prefix . 'location_zip',
				'type' => 'text',
			),
			array(
				'name' => 'Phone',
				'desc' => 'Phone number for this location',
				'id'   => $prefix . 'location_phone',
				'type' => 'text',
			),
			array(
				'name' => 'Email',
				'desc' => 'General contact email address for this location',
				'id'   => $prefix . 'location_email',
				'type' => 'text',
			),
			array(
				'name' => 'Location Discription',
				'desc' => 'Add any description about this location that you would like displayed on this locations page.',
				'id' => $prefix . 'location_desc',
				'type' => 'wysiwyg',
				'options' => array(
					'wpautop'		=> 'true',
					),
			),
			array(
				'name' => 'Display Location Staff',
				'desc' => 'Show/Hide this location\'s staff',
				'id' => $prefix . 'location_show_staff',
				'type' => 'radio_inline',
				'options' => array(
					array('name' => 'Show', 'value' => '1'),
					array('name' => 'Hide', 'value' => '2'),			
				),
				'std' => '1'
			),

		),
	);

	return $meta_boxes;
}

add_action( 'init', 'saal_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function saal_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}