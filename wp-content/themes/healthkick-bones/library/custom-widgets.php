<?php
/* Custom Widgets for this theme */

/*********************
Social Buttons
*********************/

class Social_Buttons_Widget extends WP_Widget {
	function Social_Buttons_Widget() {
		$widget_options = array(
			'classname' => 'social_buttons',
		    'description' => 'Facebook & Twitter Social buttons' 
		);

	    parent::WP_Widget("social_buttons", "Social Buttons", $widget_options);

	}
	 
	public function form( $instance ) {
		//get links from Theme Options if available
		global $smof_data;
		$fb = $smof_data['fb_link'];
		$twit = $smof_data['twitter_link'];

		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'facebook_url' => $fb, 'twitter_url' => $twit ) );
		$title = $instance['title'];
		$facebook_url = $instance['facebook_url'];
		$twitter_url = $instance['twitter_url'];
		?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo  $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title ?>"   />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('facebook_url'); ?>">Facebook Url</label>
		 	<input id="<?php echo $this->get_field_id( 'facebook_url' ); ?>" name="<?php   echo $this->get_field_name( 'facebook_url' ); ?>" type="text" value="<?php echo   $facebook_url ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('twitter_url'); ?>">Twitter Url</label>
		  	<input id="<?php echo $this->get_field_id( 'twitter_url' ); ?>" name="<?php   echo $this->get_field_name( 'twitter_url' ); ?>" type="text" value="<?php echo   $twitter_url ?>" />
		</p>
		<?php
	}
	 
	public function update( $new_instance, $old_instance ) {
		$instance = array(); 
		$instance['title'] = $new_instance['title'];
		$instance['facebook_url'] = $new_instance['facebook_url'];
		$instance['twitter_url'] = $new_instance['twitter_url'];
		return $instance;
	}
	 
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$facebook_url = apply_filters( 'widget_facebook_url', $instance['facebook_url'] );
		$twitter_url = apply_filters( 'widget_twitter_url', $instance['twitter_url'] );
		
		 
		echo $before_widget;
		echo '<!-- '.$title.' -->';
		echo '<div class="social-homepage clearfix">';
		echo '<ul>';
		if ($facebook_url != '') {
			echo '<li class="facebook-findus"><a href="'.$facebook_url.'">Find us on Facebook</a></li>';
		}
		if ($twitter_url != '') {
			echo '<li class="twitter-followus last"><a href="'.$twitter_url.'">Follow us on Twitter</a></li>';
		}
		echo '</ul>';
		echo '</div> <!-- social-homepage -->';
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("Social_Buttons_Widget");'));


/*********************
Deal of the Month Widget
*********************/

class DOTD_Widget extends WP_Widget {
	function DOTD_Widget() {
		$widget_options = array(
			'classname' => 'dotd',
		    'description' => 'Deal of the Month Widget' 
		);

	    parent::WP_Widget("dotd", "Deal of the Month", $widget_options);

	}
	 
	public function form( $instance ) {

		// $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'facebook_url' => $fb, 'twitter_url' => $twit ) );
		// $title = $instance['title'];
	
		echo '<p>This widget will display the deal of the day as set in the admin panel.</p>';
		
	}
	 
	public function update( $new_instance, $old_instance ) {
		$instance = array(); 
		return $instance;
	}
	 
	public function widget( $args, $instance ) {
		extract( $args );		
		$current_date = time();
		$deals = new WP_Query(array(
			'post_type'		=>	'deal',
			'posts_per_page'=>	'1',
			'meta_query'	=>	array(
				array(
					'key'	=>	'_saal_deal_start_date',
					'value'	=>	$current_date,
					'compare'	=>	'<='
					),
				array(
					'key'	=>	'_saal_deal_exp_date',
					'value'	=>	$current_date,
					'compare'	=>	'>='
					)
				)
			));

		echo $before_widget; ?>
		<div class="dealoftheday clearfix">
		<h3>Deal of the Month</h3>
		
		<?php 
			if($deals->have_posts()): 
					while ($deals->have_posts()) : $deals->the_post();
					global $post ;
					$meta = get_post_meta($post->ID);
					// print_r($meta);
					
					$offer = isset($meta['_saal_deal_offer'][0]) ? $meta['_saal_deal_offer'][0] : null;
					$subtext = isset($meta['_saal_deal_subtext'][0]) ? $meta['_saal_deal_subtext'][0] : null;
					$link = isset($meta['_saal_deal_link'][0]) ? $meta['_saal_deal_link'][0] : null;
					$imgID = isset($meta['_saal_deal_image_id'][0]) ? $meta['_saal_deal_image_id'][0] : null;
					$imgArray = wp_get_attachment_image_src($imgID, 'dotw-thumb');
					$img = $imgArray[0];
						
					echo '<div class="deal clearfix">';
						if ($link && $link !='') { echo '<a href="'.$link.'">'; }
						if ($img && $img !='') { echo '<img src="'.$img.'">'; }
						echo '<div class="offer-content">';
						if ($offer && $offer !='') { echo '<span class="offer">'.$offer.'</span>'; }
						if ($subtext && $subtext !='') { echo '<span class="subtext">'.$subtext.'</span>'; }
						if ($link && $link !='') { echo '<span class="clickhere">Click here to purchase</span>'; }
						echo '</div> <!-- offer-content -->';
						if ($link) { echo '</a>'; }
					echo '</div><!--  deal -->';

				endwhile; 
			else :
				echo '<p>We are currently updating our deals, so check back soon!</p><p><a href="'.get_permalink( woocommerce_get_page_id( 'shop' )).'"" class="button">View all of our products</a></p>';
			endif;
			// echo $current_date; 
		?>
		
		</div> <!-- dealoftheday -->
		<?php echo $after_widget;
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("DOTD_Widget");'));

/*********************
Lates HKN News
*********************/

class Latest_HKN_News_Widget extends WP_Widget {
	function Latest_HKN_News_Widget() {
		$widget_options = array(
			'classname' => 'latest-hkn-news',
		    'description' => 'Latest News posts from the HKN Blog' 
		);

	    parent::WP_Widget("latest-hkn-news", "Latest News posts from the HKN Blog", $widget_options);

	}
	 
	public function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Latest Healthkick News', 'shownum' => '3' ) );
		$title = $instance['title'];
		$shownum = $instance['shownum'];

		?>
	
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo  $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title ?>"   />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('shownum'); ?>">Number of posts to display</label>
			<input id="<?php echo $this->get_field_id( 'shownum' ); ?>" name="<?php echo  $this->get_field_name( 'shownum' ); ?>" type="text" value="<?php echo $shownum ?>"   />
		</p>

		<?php 
		
	}
	 
	public function update( $new_instance, $old_instance ) {
		$instance = array(); 
		$instance['title'] = $new_instance['title'];
		$instance['shownum'] = $new_instance['shownum'];
		return $instance;
	}
	 
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$shownum = apply_filters( 'widget_shownum', $instance['shownum'] );

		$hknposts = new WP_Query(array(
			'posts_per_page'	=> $shownum,
			'orderby' 			=> 'date',
			'order' 			=> 'desc',
			));
				
		 
		echo $before_widget;
		echo '<div class="latest-news clearfix">';
		echo '<h3>'.$title.'</h3>';

		if($hknposts->have_posts()):
			echo '<ul class="news-list">';

			while($hknposts->have_posts()):$hknposts->the_post(); ?>
				 <li>
					<a href="<?php echo get_permalink();?>">
						<div class="news-list-date">
							<span class="month"><?php the_time('M');?></span>
							<span class="day"><?php the_time('d');?></span>
							<span class="year"><?php the_time('Y');?></span>
						</div> <!-- news-list-date -->
						<div class="news-list-title">
							<?php the_title();?>
							<span>Written by <?php echo get_the_author();?></span>
						</div> <!-- news-list-title -->
					</a>
				<?php echo '</li>';
			endwhile;

			echo '</ul>';
			echo '<a href="'.get_page_link('16').'" class="more-link">Read more nutrition news</a>';
		else:
			echo '<p>There are no new posts at this time.</p>';
		endif;

		echo '</div> <!-- latest-news -->';
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("Latest_HKN_News_Widget");'));

/*********************
Email Newsletter Signup Widget
*********************/

class Email_Signup_Widget extends WP_Widget {
	function Email_Signup_Widget() {
		$widget_options = array(
			'classname' => 'email_signup_widget',
		    'description' => 'Email Newsletter Signup Widget' 
		);

	    parent::WP_Widget("email_signup_widget", "Email Newsletter Signup", $widget_options);

	}
	 
	public function form( $instance ) {
		global $smof_data;
		$newsletter = $smof_data['email_newsletter_form'];
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'formcode' => $newsletter, 'desc'=>'') );
		$title = $instance['title'];
		$formcode = $instance['formcode']; 
		$desc = $instance['desc']; ?>
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo  $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title ?>"   />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('desc'); ?>">Intro paragraph</label>
			<textarea id="<?php echo $this->get_field_id( 'desc' ); ?>" name="<?php echo  $this->get_field_name( 'desc' ); ?>" style="width:100%;"><?php echo $desc ?></textarea>
		</p>

		<input type="hidden" value="<?php $formcode; ?>" id="<?php $this->get_field_id( 'formcode' );?> name="<?php echo  $this->get_field_name( 'formcode' ); ?>">

		<?php echo '<p>This widget will display the email newsletter signup form (if set in the website options panel).</p>'; 

		if ($formcode) { echo '<p style="color:green;">There is currently a form code entered, so you should be good to go!</p>'; } 
		else { 
			echo '<p style="color:red; font-weight:bold;">You will need to enter html code for the email signup form in your <a href="'.admin_url('themes.php?page=optionsframework').'">Options Panel</a></p>';
		}		
	}
	 
	public function update( $new_instance, $old_instance ) {
		global $smof_data;
		$formcode = $smof_data['email_newsletter_form'];
		$instance = array(); 
		$instance['title'] = $new_instance['title'];
		$instance['desc'] = $new_instance['desc'];
		$instance['formcode'] = $formcode;
		return $instance;
	}
	 
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$formcode = $instance['formcode'];
		$desc = $instance['desc'];	
		 
		echo $before_widget;
		echo '<div class="social-homepage clearfix">';
		
			echo '<h3>'.$title.'</h3>';
		
			if (isset($desc) && $desc !='') { echo wpautop($desc); }
			if (isset($formcode) && $formcode != ''){ echo $formcode; }
		echo '</div>';
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("Email_Signup_Widget");'));

?>
