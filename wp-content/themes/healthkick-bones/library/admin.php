<?php
/*
This file handles the admin area and functions.
You can use this file to make changes to the
dashboard. Updates to this page are coming soon.
It's turned off by default, but you can call it
via the functions file.

Developed by: Eddie Machado
URL: http://themble.com/bones/

Special Thanks for code & inspiration to:
@jackmcconnell - http://www.voltronik.co.uk/
Digging into WP - http://digwp.com/2010/10/customize-wordpress-dashboard/

*/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	remove_meta_box('dashboard_right_now', 'dashboard', 'core');    // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget

	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //

	// removing plugin dashboard boxes
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget

	/*
	have more plugin widgets you'd like to remove?
	share them with us so we can get a list of
	the most commonly used. :D
	https://github.com/eddiemachado/bones/issues
	*/
}

/*
Now let's talk about adding your own custom Dashboard widget.
Sometimes you want to show clients feeds relative to their
site's content. For example, the NBA.com feed for a sports
site. Here is an example Dashboard Widget that displays recent
entries from an RSS Feed.

For more information on creating Dashboard Widgets, view:
http://digwp.com/2010/10/customize-wordpress-dashboard/
*/

// Dashboard intro
function saal_dashboard_help() {
	echo '<p>From this admin panel you can control all of the editable content of your website. Simply use the navigation menues to the left to target and edit the content you wish to change/update.  You can also update editable content from the front-end of the website once you have logged in by clicking the name of your company in the upper left-hand corner of the screen in the top admin bar.</p>';
	echo 'Need help? Let us konow! <br><br><b>Wes DeKoninck</b><br><b>Email: </b><a href="mailto:wes@saal.net?Subject=Support for ' . get_bloginfo('url') . '">wes@saal.net</a><br><b>Phone:</b> 260.432.7225 - x117<br><b>Cell:</b> 260.437.0548</p><br>SAAL Advertising<br>6528 Constitution Dr.<br>Fort Wayne, IN 46804<br><br>';
}

// calling all custom dashboard widgets
function bones_custom_dashboard_widgets() {
	wp_add_dashboard_widget('saal_dashboard_help', __('Welcome to your Admin Dashboard', 'bonestheme'), 'saal_dashboard_help');
}


// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');
// adding any custom widgets
add_action('wp_dashboard_setup', 'bones_custom_dashboard_widgets');


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it

//Updated to proper 'enqueue' method
//http://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
function bones_login_css() {
	wp_enqueue_style( 'bones_login_css', get_template_directory_uri() . '/library/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function bones_login_url() {  return home_url(); }

// changing the alt text on the logo to show your site name
function bones_login_title() { return get_option('blogname'); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );
add_filter('login_headerurl', 'bones_login_url');
add_filter('login_headertitle', 'bones_login_title');


/************* CUSTOMIZE ADMIN *******************/

/*
I don't really recommend editing the admin too much
as things may get funky if WordPress updates. Here
are a few funtions which you can choose to use if
you like.
*/

// Custom Backend Footer
function bones_custom_admin_footer() {
	_e('<span id="footer-thankyou">Developed by <a href="http://saal.net" target="_blank">SAAL Advertising</a></span>.');
}

// adding it to the admin area
add_filter('admin_footer_text', 'bones_custom_admin_footer');

// Disable dashboard menu items that are not necessary if not admin 
if (!(current_user_can('administrator'))) {
	function remove_menu_items() {
	  global $menu;
	  $restricted = array(__('Tools'), __('Links'), __('Appearance'));
	  end ($menu);
	  while (prev($menu)){
		$value = explode(' ',$menu[key($menu)][0]);
		if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
		  unset($menu[key($menu)]);}
		}
	  }
add_action('admin_menu', 'remove_menu_items');
} 


// Adding dashboard menu item for theme options
add_action( 'admin_menu', 'register_my_custom_menu_page' );
function register_my_custom_menu_page(){
    add_menu_page( 'Website Options', 'Website Options', 'edit_pages', 'themes.php?page=optionsframework', '', 'dashicons-performance' , 4 );
}

// Adding dashboard menu item for navigation options
add_action( 'admin_menu', 'register_my_nav_menu_page' );

function register_my_nav_menu_page(){
    add_menu_page( 'Navigation', 'Navigation', 'edit_pages', 'nav-menus.php', '', 'dashicons-feedback' , 3 );
}

function saal_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu( array(
		'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
		'id' => 'website-options', // link ID, defaults to a sanitized title value
		'title' => __('Website Options'), // link title
		'href' => admin_url( 'themes.php?page=optionsframework'), // name of file
		'meta' => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	));
}
add_action( 'wp_before_admin_bar_render', 'saal_admin_bar_render' );

// Custom Theme Options
require_once('theme-options/admin/index.php');

?>
