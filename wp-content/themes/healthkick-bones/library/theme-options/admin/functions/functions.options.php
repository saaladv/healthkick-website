<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       
	
		//Select Arrays
		$of_options_cc 	= array("visa" => "Visa","mc" => "Mastercard","discover" => "Discover","amex" => "American Express","paypal" => "Paypal");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

$of_options[] = array( 	"name" 		=> "General Settings",
						"type" 		=> "heading"
				);
					
$of_options[] = array( 	"name" 		=> "Welcome",
						"desc" 		=> "",
						"id" 		=> "introduction",
						"std" 		=> "<h3 style=\"margin: 0 0 10px;\">Welcome to your website Options Panel.</h3>
						From this options panel, youc an manage various parts of your website.<br><br>If you should have any questions about these options or how to change/use them, please contact Wes DeKoninck at <a href=\"mailto:wes@saal.net\">wes@saal.net</a> or 260.432.7225 x117",
						"icon" 		=> true,
						"type" 		=> "info"
				);

$of_options[] = array( 	"name" 		=> "Header Tagline",
						"desc" 		=> "This tagline is displayed at the top of the website beside the logo.",
						"id" 		=> "header_tagline",
						"std" 		=> "Your nutrition and supplement headquarters since 1998!",
						"type" 		=> "text"
				);

$of_options[] = array( 	"name" 		=> "Show Knowledge Banner",
						"desc" 		=> "Control display of the Knowledge Banner on the homepage.",
						"id" 		=> "home_show_knowledge",
						"std" 		=> 1,
						"on" 		=> "Show",
						"off" 		=> "Hide",
						"folds"		=> 1,
						"type" 		=> "switch"
				);

$of_options[] = array( 	"name" 		=> "Knowledge Banner Text",
						"desc" 		=> "This text is displayed in the Knowledge Banner section of the homepage.",
						"id" 		=> "home_knowledge",
						"std" 		=> "You can buy health supplements many different places, but at Healthkick Nutrition Centers, you can be sure you are getting the RIGHT supplements for the goals you are wanting to achieve.",
						"fold"		=> 'home_show_knowledge',
						"type" 		=> "textarea"
				);

$of_options[] = array( 	"name" 		=> "Knowledge Banner Link",
						"desc" 		=> "This is the page link for the knowledge banner <strong>Meet the Staff</strong> button.  Leave blank for no button",
						"id" 		=> "home_knowledge_link",
						"std" 		=> "",
						"fold"		=> 'home_show_knowledge',
						"type" 		=> "text"
				);

$of_options[] = array( 	"name" 		=> "Credit Card Logos",
						"desc" 		=> "Control display of the credit card logos in the footer",
						"id" 		=> "show_cc",
						"std" 		=> 1,
						"folds"		=> 1,
						"on"		=> "Show",
						"hide"		=> "Hide",
						"type" 		=> "switch"
				);

$of_options[] = array( 	"name" 		=> "Credit Card Logos",
						"desc" 		=> "Select which Credit Card logos to display",
						"id" 		=> "cc_types",
						"std" 		=> array("visa","mc", "discover", "amex", "paypal"),
						"type" 		=> "multicheck",
						"fold"		=> 'show_cc',
						"options" 	=> $of_options_cc
				);

$of_options[] = array( 	"name" 		=> "Footer Copyright Text",
						"desc" 		=> "The year and business name are added dynamically from the website.  You can control the remaining text here.",
						"id" 		=> "footer_copyright",
						"std" 		=> "All rights reserved.",
						"type" 		=> "text"
				);


/****************************
Social Settings
*****************************/
$of_options[] = array( 	"name" 		=> "Social Settings",
						"type" 		=> "heading"
				);

$of_options[] = array( 	"name" 		=> "Facebook",
						"desc" 		=> "Enter the URL of your Facebook Fan Page.",
						"id" 		=> "fb_link",
						"std" 		=> "",
						"type" 		=> "text"
				);

$of_options[] = array( 	"name" 		=> "Twitter",
						"desc" 		=> "Enter the URL of your Twitter Page.",
						"id" 		=> "twitter_link",
						"std" 		=> "",
						"type" 		=> "text"
				);
$of_options[] = array( 	"name" 		=> "Email Newsletter",
						"desc" 		=> "HTML for email newsletter signup form.",
						"id" 		=> "email_newsletter_form",
						"std" 		=> "",
						"type" 		=> "textarea"
				);

/****************************
Homepage Slider Settings
*****************************/
$of_options[] = array( 	"name" 		=> "Home Slider Settings",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-slider2.png"
				);

$of_options[] = array( 	"name" 		=> "Slider Options",
						"desc" 		=> "Creat/Sort homepages image slider images",
						"id" 		=> "homepage_slider",
						"std" 		=> "",
						"type" 		=> "slider"
				);

$of_options[] = array( 	"name" 		=> "Admin Settings",
						"type" 		=> "heading"
				);
$of_options[] = array( 	"name" 		=> "Website Analytics",
						"id" 		=> "google_analytics",
						"desc"		=> "Google Analytics tracking code to be placed in the header of the website for tracking website traffic.",
						"std" 		=> "",
						"type" 		=> "textarea"
				);


				
	}//End function: of_options()
}//End chack if function exists: of_options()
?>
