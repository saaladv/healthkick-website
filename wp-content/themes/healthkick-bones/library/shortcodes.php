<?php

/* --------------------------------------------------
	Custom Shortcodes
---------------------------------------------------*/


/* --------------------------------------------------
	[line]
	- Display a simple separation line
---------------------------------------------------*/

function line_shortcode( $atts ) {
	return '<div class="seperator"></div>';
}
add_shortcode( 'line', 'line_shortcode' );	

function saal_shortcode_wc_categories( $atts ) {
	the_widget('WC_Widget_Product_Categories', array(
		'orderby'		=> 'name',
		'count'			=>  0,
		'hierarchical'	=> 	true,
		'dropdown'		=> 0
	));
}
add_shortcode( 'wc_categories', 'saal_shortcode_wc_categories' );	
		
/* --------------------------------------------------
	[staff-directory]
	- Display all staff members
---------------------------------------------------*/
function saal_shortcode_staff_directory( $atts ) {
	global $post;
	$staff = new WP_Query(array(
		'post_type' => 'staff',
		'posts_per_page' => -1
	));

	if ($staff->have_posts()):
		echo '<ul class="staff-list clearfix">';
		while($staff->have_posts()):$staff->the_post();
			$meta = get_post_meta($post->ID);
			$staff_position = (isset($meta['_saal_position'][0]) && $meta['_saal_position'][0] != '')?$meta['_saal_position'][0]:null;
			$staff_email = (isset($meta['_saal_staff_email'][0]) && $meta['_saal_staff_email'][0] != '')?$meta['_saal_staff_email'][0]:null;
			$staff_locationID = (isset($meta['_saal_staff_location'][0]) && $meta['_saal_staff_location'][0] != '')?$meta['_saal_staff_location'][0]:null;
			$locationName = get_the_title($staff_locationID);
			$locationMeta = get_post_meta($staff_locationID);
			$locationPhone = isset($locationMeta['_saal_location_phone'][0]) ? $locationMeta['_saal_location_phone'][0] : null;

			echo '<li class="clearfix">';
				echo '<p><span class="staff-name">'.get_the_title().'</span>';
					echo ($staff_position)?'<span class="staff-position">'.$staff_position.'</span>':'';
					echo ($locationName)?'<span class="staff-location"><a href="'.get_permalink($locationId).'">'.$locationName.'</a>':'';
						echo ($locationPhone)?'<br>'.$locationPhone:'';
					echo '</span>';
					echo ($email)?'<a href="mailto:'.$email.'" class="button small">Email</a>':'';
				echo '</p>';
			echo '</li>';
		endwhile;
		echo '</ul>';
	endif;

	wp_reset_postdata();
}
add_shortcode( 'staff-directory', 'saal_shortcode_staff_directory' );

?>
