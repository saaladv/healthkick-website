<?php

/* Creating Custom Post Type for Staff | CPT Type: staff */
	add_action( 'init', 'saal_reg_cpt_staff' );
	
	function saal_reg_cpt_staff() {
		register_post_type( 'staff',
			array(
				'labels' => array(
					'name' => __( 'Staff' ),
					'singular_name' => __( 'Staff' ),
					'add_new' => __( 'Add New' ),
					'add_new_item' => __( 'Add New Staff' ),
					'edit' => __( 'Edit' ),
					'edit_item' => __( 'Edit Staff' ),
					'new_item' => __( 'New Staff' ),
					'view' => __( 'View Staff' ),
					'view_item' => __( 'View Staff' ),
					'search_items' => __( 'Search Staff' ),
					'not_found' => __( 'No Staff found' ),
					'not_found_in_trash' => __( 'No Staff found in Trash' ),
					'parent' => __( 'Parent Staff' ),
	
				),
				'public' => true,
				'show_ui' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'description' => __( 'Staff' ),
				'menu_position' => 4,
				'query_var' => true,
				'supports' => array( 'title', 'thumbnail'),
				'rewrite' => array( 'slug' => 'staff-member', 'with_front' => false ),
				'can_export' => true,
				'menu_icon' => 'dashicons-businessman'			
			)
		);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	/* Creating Custom Post Type for Location | CPT Type: location */
	add_action( 'init', 'saal_reg_cpt_location' );
	
	function saal_reg_cpt_location() {
		register_post_type( 'location',
			array(
				'labels' => array(
					'name' => __( 'Locations' ),
					'singular_name' => __( 'Location' ),
					'add_new' => __( 'Add New' ),
					'add_new_item' => __( 'Add New Location' ),
					'edit' => __( 'Edit' ),
					'edit_item' => __( 'Edit Location' ),
					'new_item' => __( 'New Location' ),
					'view' => __( 'View Locations' ),
					'view_item' => __( 'View Location' ),
					'search_items' => __( 'Search Locations' ),
					'not_found' => __( 'No Locations found' ),
					'not_found_in_trash' => __( 'No Locations found in Trash' ),
					'parent' => __( 'Parent Location' ),
	
				),
				'public' => true,
				'show_ui' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'description' => __( 'Location' ),
				'menu_position' => 4,
				'query_var' => true,
				'supports' => array( 'title', 'thumbnail'),
				'rewrite' => array( 'slug' => 'healthkick-locations', 'with_front' => false ),
				'can_export' => true,
				'menu_icon' => 'dashicons-location-alt'
			)
		);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	/* Creating Custom Post Type for Deal | CPT Type: deal */
	add_action( 'init', 'saal_reg_cpt_deal' );
	
	function saal_reg_cpt_deal() {
		register_post_type( 'deal',
			array(
				'labels' => array(
					'name' => __( 'Deal of the Month' ),
					'singular_name' => __( 'Deal' ),
					'add_new' => __( 'Add New' ),
					'add_new_item' => __( 'Add New Deal' ),
					'edit' => __( 'Edit' ),
					'edit_item' => __( 'Edit Deal' ),
					'new_item' => __( 'New Deal' ),
					'view' => __( 'View Deals' ),
					'view_item' => __( 'View Deal' ),
					'search_items' => __( 'Search Deals' ),
					'not_found' => __( 'No Deals found' ),
					'not_found_in_trash' => __( 'No Deals found in Trash' ),
					'parent' => __( 'Parent Deal' ),
	
				),
				'public' => true,
				'show_in_nav_menus' => false,
				'show_ui' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => true,
				'description' => __( 'Deal' ),
				'menu_position' => 4,
				'query_var' => true,
				'supports' => array( 'title'),
				'can_export' => true,
				'menu_icon' => 'dashicons-awards'		
			)
		);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	/* Creating Custom Post Type for Testimonial | CPT Type: testimonial */
	add_action( 'init', 'saal_reg_cpt_testimonial' );
	
	function saal_reg_cpt_testimonial() {
		register_post_type( 'testimonial',
			array(
				'labels' => array(
					'name' => __( 'Testimonials' ),
					'singular_name' => __( 'Testimonial' ),
					'add_new' => __( 'Add New' ),
					'add_new_item' => __( 'Add New Testimonial' ),
					'edit' => __( 'Edit' ),
					'edit_item' => __( 'Edit Testimonial' ),
					'new_item' => __( 'New Testimonial' ),
					'view' => __( 'View Testimonials' ),
					'view_item' => __( 'View Testimonial' ),
					'search_items' => __( 'Search Testimonials' ),
					'not_found' => __( 'No Testimonials found' ),
					'not_found_in_trash' => __( 'No Testimonials found in Trash' ),
					'parent' => __( 'Parent Testimonial' ),
	
				),
				'public' => true,
				'show_ui' => true,
				'publicly_queryable' => true,
				'exclude_from_search' => false,
				'description' => __( 'Testimonial' ),
				'menu_position' => 4,
				'query_var' => true,
				'supports' => array( 'title', 'editor', 'thumbnail'),
				'menu_icon' => 'dashicons-format-quote',
				'can_export' => true			
			)
		);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
		
	/* Styling for the custom post type icon*/
	add_action( 'admin_head', 'saal_admin_icons' );
	function saal_admin_icons() {
	    ?>	    
	    <style type="text/css" media="screen">
								
			/* Large Icon for Post Page */
			#icon-edit.icon32-posts-staff { background: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/admin-icons/user-32.png) no-repeat; }
			#icon-edit.icon32-posts-location { background: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/admin-icons/store-32.png) no-repeat; }
			#icon-edit.icon32-posts-deal { background: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/admin-icons/money-32.png) no-repeat; }
			#icon-edit.icon32-posts-testimonial { background: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/admin-icons/balloon-quotation-32.png) no-repeat; }
	    </style>
	<?php }	
	

	/* Custom Meta Boxes */
	require_once('metabox/metaboxes.php');


	/* Change title placeholder text for custom post types */
	function custom_title_text( $title ){
		$screen = get_current_screen();
		$cptype = $screen->post_type;
		
		switch($cptype) {
			case "staff":
				$title = 'Staff Member Name';
				return $title;
				break;
			case "location":
				$title = 'Location Name';
				return $title;
				break;
			case "deal":
				$title = 'Deal Title';
				return $title;
				break;
			case "testimonial":
				$title = 'Client Name';
				return $title;
				break;
			default:
				$title = 'Enter Title Here';
				return $title;		
		}
	}
	add_filter( 'enter_title_here', 'custom_title_text' );
		
		

?>
