<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		
		<?php global $woocommerce, $smof_data; ?>
		<?php $ga = (isset($smof_data['google_analytics']) && $smof_data['google_analytics'] != '')?$smof_data['google_analytics']:null;?>

		<?php echo ($ga)?$ga:'';?>

	</head>

	<body <?php body_class(); ?>>

		<div id="container">

			<header class="header" role="banner">

				<div id="inner-header" class="wrap clearfix">

					<div class="clearfix">
						<div class="fourcol first ">
						<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><img src="<?php bloginfo('template_directory'); ?>/library/images/hkn-logo-300.png"></a>
						</div> <!-- fourcol -->

						<div class="fourcol slogan">
							<p><?php if ($smof_data['header_tagline']) { echo $smof_data['header_tagline']; }?>
								<?php $locationCount = wp_count_posts('location'); ?>
								<span>Now with <?php echo $locationCount->publish;?> Locations!</span>
							</p>
						</div> <!-- fourcol -->	

						<div class="fourcol last">
							<ul class="shop-links clearfix">
								<li>
									<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
										<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?>
									</a>
								</li>
								
								<?php wp_nav_menu(array('theme_location' => 'cart-top-links', 'sort_column' => 'menu_order', 'items_wrap'=>'%3$s','container' => false)); ?>
							</ul> <!-- shop-links -->							

							<div class="clearfix text-right">
								<div class="searchbar">
									<form role="search" method="get" id="searchform" action="<?php echo home_url();?>">
										<label class="screen-reader-text" for="s">Search for:</label>
										<input type="text" value="" name="s" id="s" placeholder="Search Item # or Keyword">
										<input type="submit" id="searchsubmit" value="Search">
										<input type="hidden" name="post_type" value="product">
									</form>
								</div> <!-- searchbar -->

								<ul class="social-header">
									<?php 
										$fb_link = $smof_data['fb_link']; 
										$twitter_link = $smof_data['twitter_link'];
									
										if ($fb_link) {
											echo '<li class="facebook"><a href="'.$fb_link.'">'.get_bloginfo('name').' on Facebook</a></li>';
										}
										if ($twitter_link) {
											echo '<li class="twitter"><a href="'.$twitter_link.'">'.get_bloginfo('name').' on Twitter</a></li>';
										}
									?>
								</ul>
							</div> <!-- clearfix -->
						</div> <!-- fourcol -->	
					</div> <!-- clearfix -->
					
					<a href="" class="mobile-nav-ic">Navigation Menu</a>
					<nav role="navigation" class="mobile-nav">
						<?php bones_main_nav(); ?>
					</nav>

				</div> <!-- end #inner-header -->

			</header> <!-- end header -->
