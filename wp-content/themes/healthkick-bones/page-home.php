<?php
/*
Template Name: Custom Page Example
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="twelvecol first clearfix" role="main">

							<?php 
								$slides =  $smof_data['homepage_slider']; //get slides array


								if ($slides) {?>
									<div class="hero flexslider clearfix">
									<ul class="slides">
										<?php foreach ($slides as $slide) { 
											$title = $slide['title'];
											$img = $slide['url'];
											$link = $slide['link'];

											echo '<li>';
												if ($link) {
													echo '<a href="'.$link.'">';
												}
												if($img) {
													echo '<img src="'.$img.'" alt="'.$title.'">';
												}
												if($link) {
													echo '</a>';
												}
											echo '</li>';
										} ?>
									</ul>
								</div> <!-- hero -->
							<?php } ?>

							<?php global $woocommerce_loop, $woocommerce; ?> <!-- load WooCommerce -->

							<div class="fourcol location-box-home first clearfix">
								<?php $locationCount = wp_count_posts('location'); ?>
								
								<h3>Store Locations in Indiana &amp; Texas</h3>
								<p>Feel free to visit one of our <?php echo $locationCount->publish;?>  HealthKick stores to talk with our knowledgable staff and get the products you need to meet your fitness and nutrition goals!</p>
								<a href="<?php echo get_page_link(17);?>" class="button">Find your closest HKN Store!</a>
							</div> <!-- fourcol -->
						
							<div class="product-slider eightcol last clearfix">
								<div class="tabs clearfix">
									<ul class="tab-titles">
										<li><a href="#tabs-1">Featured Products</a></li>
										<li><a href="#tabs-2">New Products</a></li>
										<li><a href="#tabs-3">Deals</a></li>
										<li class="tab-text">Choose type of products to view</li>
									</ul>
									<div id="tabs-1" class="products-flexslider">									
											<!-- featured products -->
											<?php
												$featured_p = new WP_Query(array(
													'post_type'	=> 'product',
													'post_status' => 'publish',
													'ignore_sticky_posts'	=> 1,
													'posts_per_page' => 12,
													'orderby' => 'date',
													'order' => 'desc',
													'meta_query' => array(
														array(
															'key' => '_visibility',
															'value' => array('catalog', 'visible'),
															'compare' => 'IN'
														),
														array(
															'key' => '_featured',
															'value' => 'yes'
														)
													)
												));

												$woocommerce_loop['columns'] = 4;

												if ( $featured_p->have_posts()) :

													echo '<ul class="slides products">';

													while ($featured_p->have_posts()) : $featured_p->the_post();

													
														woocommerce_get_template_part( 'content', 'product' );


													endwhile;
													echo '</ul>';
												else:
													echo '<p>There are no featured products to display at this time.  Please check back soon!</p>';
												endif;

												wp_reset_postdata();
											?>
									</div> <!-- tabs-1 -->
									<div id="tabs-2" class="products-flexslider">
											<!-- new products -->
											<?php
												$new_p = new WP_Query(array(
													'post_type'	=> 'product',
													'post_status' => 'publish',
													'ignore_sticky_posts'	=> 1,
													'posts_per_page' => 12,
													'orderby' => 'date',
													'order' => 'desc',
													'meta_query' => array(
														array(
															'key' => '_visibility',
															'value' => array('catalog', 'visible'),
															'compare' => 'IN'
														)
													)
												));

												$woocommerce_loop['columns'] = 4;

												if ( $new_p->have_posts()) :
													echo '<ul class="slides">';
													while ($new_p->have_posts()) : $new_p->the_post();

													
														woocommerce_get_template_part( 'content', 'product' );


													endwhile;
													echo '</il>';
												else:
													echo '<p>There are no new products to display at this time.  Please check back soon!</p>';
												endif;

												wp_reset_postdata();
											?>
										</ul>
									</div> <!-- tabs-2 -->
									<div id="tabs-3" class="products-flexslider">
											<!-- sale products -->
											<?php 
												$product_ids_on_sale = woocommerce_get_product_ids_on_sale();

												$meta_query = array();
												$meta_query[] = $woocommerce->query->visibility_meta_query();
											    $meta_query[] = $woocommerce->query->stock_status_meta_query();
											    $meta_query   = array_filter( $meta_query );
												
												$new_p = new WP_Query(array(
													'post_type'			=> 'product',
													'post_status' 		=> 'publish',
													'no_found_rows' 	=> 1,
													'posts_per_page' 	=> 12,
													'orderby' 			=> 'date',
													'order' 			=> 'desc',
													'meta_query' 		=> $meta_query,
													'post__in'			=> $product_ids_on_sale
												));

												$woocommerce_loop['columns'] = 4;

												if ( $new_p->have_posts()) :
													echo '<ul class="slides products">';
													while ($new_p->have_posts()) : $new_p->the_post();

													
														woocommerce_get_template_part( 'content', 'product' );


													endwhile;
													echo '</ul>';
												else: 
													echo '<p>There are no new sale products to display at this time.  Please check back soon!</p>';
												endif;

												wp_reset_postdata();
											?>
										</ul>
									</div> <!-- tabs-3 -->
								</div> <!-- tabs -->
							</div> <!-- product-slider-home -->

							<?php 
								$show_knowledge = $smof_data['home_show_knowledge'];
								$btn_link = $smof_data['home_knowledge_link'];

								if ($show_knowledge == 1) { ?>
									<div class="twelvecol first knowledge-banner clearfix">
										<h3>The difference is in the <span>Knowledge...</span></h3>	

										<?php 
											$knowledge_text = $smof_data['home_knowledge'];
											if ($knowledge_text) {
												echo '<p>';
													echo stripslashes($knowledge_text);

													if ($btn_link) {
														echo '<a href="'.$btn_link.'">Meet Healthkick Nutrition\'s Knowledgable staff</a>';
													}
												echo '</p>';
											}?>

									</div> <!-- knowledge-banner -->
							<?php } ?>

							<div class="homepage-bottom-content clearfix">
								<div class="fourcol latest-news first clearfix">
									<h3>Latest Healthkick News</h3>
									<?php 
										$hknposts = new WP_Query(array(
											'posts_per_page'	=> '3',
											'orderby' 			=> 'date',
											'order' 			=> 'desc',
											));

										if($hknposts->have_posts()):
											echo '<ul class="news-list">';

											while($hknposts->have_posts()):$hknposts->the_post(); ?>
												 <li>
													<a href="<?php echo get_permalink();?>">
														<div class="news-list-date">
															<span class="month"><?php the_time('M');?></span>
															<span class="day"><?php the_time('d');?></span>
															<span class="year"><?php the_time('Y');?></span>
														</div> <!-- news-list-date -->
														<div class="news-list-title">
															<?php the_title();?>
															<span>Written by <?php echo get_the_author();?></span>
														</div> <!-- news-list-title -->
													</a>
												<?php echo '</li>';
											endwhile;
										else:
											echo '<p>There are no new posts at this time.</p>';
										endif;
										?>
									<a href="<?php echo get_page_link('16');?>" class="more-link">Read more nutrition news</a>
								</div> <!-- fourcol -->

								<div class="fourcol clearfix">
									<?php the_widget('DOTD_Widget'); //load DOTD widget ?>
								</div> <!-- fourcol -->

								<div class="fourcol social-homepage last clearfix">
									<?php 
										global $smof_data;
										$fblink = isset($smof_data['fb_link']) ? $smof_data['fb_link'] : null;
										$twitlink = isset($smof_data['twitter_link']) ? $smof_data['twitter_link'] : null;
										$newsletter = isset($smof_data['email_newsletter_form']) ? $smof_data['email_newsletter_form'] : null;
									?>
									<h3>Stay Up-to-Date!</h3>
									<ul>
										<?php if ($fblink) { 
											echo '<li class="facebook-findus"><a href="'.$fblink.'">Find us on Facebook</a></li>';
										}

										if ($twitlink) {
											echo '<li class="twitter-followus last"><a href="'.$twitlink.'">Follow us on Twitter</a></li>';
										} ?>
									</ul>

									<?php if ($newsletter) { ?>
										<h4>Signup for our Email Newsletter</h4>
										<p>Stay up-to-date and get current offers, new product information and much more!</p>

										<?php echo $newsletter; 
									} ?>
								</div> <!-- fourcol -->
							</div> <!-- homepage-bottom-content -->

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								

							<?php endwhile; endif; ?>

						</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
