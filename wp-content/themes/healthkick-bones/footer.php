			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">

					<div class="twocol first clearfix">
						<h4>Help & Support</h4>
						<?php wp_nav_menu(array('theme_location' => 'footer-help', 'menu_class' => 'clearfix', 'sort_column' => 'menu_order','depth' => 0, 'fallback_cb' => 'bones_footer_links_fallback')); ?>
					</div> <!-- twocol -->

					<div class="twocol clearfix">
						<h4>About Healthkick</h4>
						<?php wp_nav_menu(array('theme_location' => 'footer-about', 'menu_class' => 'clearfix', 'sort_column' => 'menu_order','depth' => 0, 'fallback_cb' => 'bones_footer_links_fallback')); ?>
					</div> <!-- twocol -->

					<div class="twocol clearfix">
						<h4>Connect With Us</h4>
						<?php wp_nav_menu(array('theme_location' => 'footer-connect', 'menu_class' => 'clearfix', 'sort_column' => 'menu_order','depth' => 0, 'fallback_cb' => 'bones_footer_links_fallback')); ?>
					</div> <!-- twocol -->

					<div class="twocol clearfix">
						<h4>Popular Brands</h4>
						<?php wp_nav_menu(array('theme_location' => 'footer-brands', 'menu_class' => 'clearfix', 'sort_column' => 'menu_order','depth' => 0, 'fallback_cb' => 'bones_footer_links_fallback')); ?>
					</div> <!-- twocol -->

					<div class="fourcol payment last clearfix">
						<?php 
							global $smof_data;
							$show_cc = $smof_data['show_cc']; 
								$cards = $smof_data['cc_types'];
									$visa = $cards['visa'];
									$mc = $cards['mc'];
									$discover = $cards['discover'];
									$amex = $cards['amex'];
									$paypal = $cards['paypal'];
							$copyright = $smof_data['footer_copyright'];

							if ( $show_cc ) { ?>
								<ul>
									<?php 
										if ($visa){ echo '<li class="visa-ic">Visa Card</li>'; }
										if ($mc){ echo '<li class="mastercard-ic">Mastercard</li>'; }
										if ($discover){ echo '<li class="discover-ic">Discover Card</li>'; }
										if ($amex){ echo '<li class="amex-ic">American Express</li>'; }
										if ($paypal){ echo '<li class="paypal-ic">PayPal</li>'; }
									?>
								</ul>
							<?php } ?>
						<a href=""><img src="<?php bloginfo('template_directory');?>/library/images/comodo-secure-logo.png" alt="Website Secured by Comodo"></a>
						<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. <?php if ($copyright) { echo $copyright; }?></p>
					</div> <!-- fourcol -->

				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->

		</div> <!-- end #container -->
		
		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</body>

</html> <!-- end page. what a ride! -->
