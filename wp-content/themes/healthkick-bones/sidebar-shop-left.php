				<div id="shop-left-sidebar" class="sidebar threecol first clearfix" role="complementary">

					<a id="shop-filters-btn" class="button">Shop Filters<i class="ic-plus"></i></a>

					<div id="shop-filters">
						<?php if ( is_active_sidebar( 'shop-left-sidebar' ) ) : ?>

							<?php dynamic_sidebar( 'shop-left-sidebar' ); ?>

						<?php else : ?>

							<!-- This content shows up if there are no widgets defined in the backend. -->

						<?php endif; ?>
					</div> <?php // shop-filters ?>

				</div>