<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<?php 
									$meta = get_post_meta($post->ID);
									$showtitle = isset($meta['_saal_show_page_title'][0]) ? $meta['_saal_show_page_title'][0] : null;

									if ($showtitle != 2) {
										echo '<h1 class="page-title" itemprop="headline">'.get_the_title().'</h1>';
									}?>
								
								</header> <!-- end article header -->

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <!-- end article section -->

							</article> <!-- end article -->

							<?php endwhile; endif; ?>

							<?php 
							// Retrieve Locations
							$locations = new WP_Query(array(
								'post_type'			=>	'location',
								'posts_per_page'	=>	'-1',
								'orderby' 			=>  'meta_value',
								'meta_key'			=> '_saal_location_order',
								'order'     		=> 'asc'	
								));

							if ( $locations->have_posts() ) : 
								echo '<ul class="location-list clearfix">';

								while ( $locations->have_posts()) : $locations->the_post();

									$meta = get_post_meta($post->ID);
									$address = isset($meta['_saal_location_address'][0]) ? $meta['_saal_location_address'][0] : null;
									$city = isset($meta['_saal_location_city'][0]) ? $meta['_saal_location_city'][0] : null;
									$state = isset($meta['_saal_location_state'][0]) ? $meta['_saal_location_state'][0] : null;
									$zip = isset($meta['_saal_location_zip'][0]) ? $meta['_saal_location_zip'][0] : null;
									$phone = isset($meta['_saal_location_phone'][0]) ? $meta['_saal_location_phone'][0] : null;
									$email = isset($meta['_saal_location_email'][0]) ? $meta['_saal_location_email'][0] : null;

									

									echo '<li class="clearfix">'; 

										echo '<div class="location-img">';
										if ( has_post_thumbnail()) {
											the_post_thumbnail('staff-thumb');
										} else {
											echo '<img src="'.get_stylesheet_directory_uri().'/library/images/location-temp-img.jpg" alt="'.get_the_title().'">';
										}
										echo '</div>';
										
										echo '<div class="location-details">';
											echo '<h3>'.get_the_title().'</h3>';
											echo '<p>';
												if ($address) { echo $address; }
												if ($city || $state || $zip ) { 
													echo '<br>';
													if ($city) { echo $city; }
													if ($state) { echo ', '.$state; }
													if ($zip) { echo ' '.$zip; }
												}
											echo '</p>';
											if($phone || $email) {
												echo '<p>';
												if ($phone) { echo 'Phone: '.$phone;}
												if ($email) { echo '<br>Email: <a href="mailto:'.$email.'">'.$email.'</a>';
												}
												echo '</p>';
											}
											echo '<a href="'.get_permalink().'" class="button">More about this location</a>';
										echo '</div>';

									echo '</li>';
								endwhile;

								echo '</ul>';

							endif;

							?>

						</div> <!-- end #main -->

						<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
