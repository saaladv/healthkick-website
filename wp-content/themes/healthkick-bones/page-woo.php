<?php 
	// Template Name: WooCommerce Full Width Page
	
		get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="twelvecol first clearfix" role="main">

							<?php woocommerce_breadcrumb(); ?>

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<?php 
									$meta = get_post_meta($post->ID);
									$showtitle = isset($meta['_saal_show_page_title'][0]) ? $meta['_saal_show_page_title'][0] : null;

									if ($showtitle != 2) {
										echo '<h1 class="page-title" itemprop="headline">'.get_the_title().'</h1>';
									}?>

								</header> <!-- end article header -->

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <!-- end article section -->

							</article> <!-- end article -->

						<?php endwhile; endif; ?>

						</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>