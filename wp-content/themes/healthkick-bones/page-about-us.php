<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<?php 
									$meta = get_post_meta($post->ID);
									$showtitle = isset($meta['_saal_show_page_title'][0]) ? $meta['_saal_show_page_title'][0] : null;

									if ($showtitle != 2) {
										echo '<h1 class="page-title" itemprop="headline">'.get_the_title().'</h1>';
									}?>
								
								</header> <!-- end article header -->

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <!-- end article section -->

							</article> <!-- end article -->

							<?php endwhile; endif; ?>

							<?php 
							// Retrieve Staff Members 
							$staff = new WP_Query(array(
								'post_type'			=>	'staff',
								'posts_per_page'	=>	-1,
								));

							if ( $staff->have_posts() ) : 
								echo '<h3>The Healthkick Team</h3>';

								echo '<ul class="staff-list twelvecol clearfix first">';

								while ( $staff->have_posts()) : $staff->the_post();

									$meta = get_post_meta($post->ID);
									$position = isset($meta['_saal_position'][0]) ? $meta['_saal_position'][0] : null;
									$email = isset($meta['_saal_staff_email'][0]) ? $meta['_saal_staff_email'][0] : null;
									$locationId = isset($meta['_saal_staff_location'][0]) ? $meta['_saal_staff_location'][0] : null;
									$locationName = get_the_title($locationId);

									$locationMeta = get_post_meta($locationId);
									$locationPhone = isset($locationMeta['_saal_location_phone'][0]) ? $locationMeta['_saal_location_phone'][0] : null;

									echo '<li class="clearfix">';
										if ( has_post_thumbnail()) {
											the_post_thumbnail('staff-thumb');
										}
										echo '<p><span class="staff-name">'.get_the_title().'</span>';
											if ($position) { 
												echo '<span class="staff-position">'.$position.'</span>';
											}
											if ($locationName) {
												echo '<span class="staff-location"><a href="'.get_permalink($locationId).'">'.$locationName.'</a>';
													if ($locationPhone) { 
														echo '<br>'.$locationPhone;
													}
												echo '</span>';
											}
											if ($email) {
												echo '<a href="mailto:'.$email.'" class="button small">Email</a>';
											}
										echo '</p>';
									echo '</li>';
								endwhile;

								echo '</ul>';

							endif;

							?>

						</div> <!-- end #main -->

						<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
