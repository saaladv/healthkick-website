<?php 
	//Template Name: Page - Testimonials
	get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<?php 
									$meta = get_post_meta($post->ID);
									$showtitle = isset($meta['_saal_show_page_title'][0]) ? $meta['_saal_show_page_title'][0] : null;

									if ($showtitle != 2) {
										echo '<h1 class="page-title" itemprop="headline">'.get_the_title().'</h1>';
									}?>
								
								</header> <!-- end article header -->

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
								</section> <!-- end article section -->

							</article> <!-- end article -->

							<?php endwhile; endif; ?>

					
							<?php
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$testimonials = new WP_Query(array(
									'post_type' => 'testimonial',
									'order' => 'desc',
									'orderby' => 'date',
									'posts_per_page' => 7,
									'paged' => $paged
									));

								if ($testimonials->have_posts()) :
									echo '<article>';
									echo '<ul class="post-list testimonials-list">';
									while($testimonials->have_posts()) : $testimonials->the_post();?>
										<li>
											<section class="entry-content">
												<?php the_content();?>
												<p><span class="testimonial-name">- <?php the_title();?></span></p>
											</section>
										</li>
									<?php endwhile;
									echo '</ul>';
									echo '</article>';
								endif;

							?>

							<?php if (function_exists('bones_page_navi')) { ?>
											<?php bones_page_navi('','',$testimonials); ?>
									<?php } else { ?>
											<nav class="wp-prev-next">
													<ul class="clearfix">
														<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
														<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
													</ul>
											</nav>
									<?php } ?>

						</div> <!-- end #main -->

						<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
