<?php
/**
 * Email Addresses
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Load colours
$text 		= get_option( 'woocommerce_email_text_color' );

$body_content_h2 = "
	color: " . esc_attr( $text ) . ";
	margin:0 0 1em;
	padding: 1em 0 0.5em;
	display:block;
	font-family:Arial;
	font-size:18px;
	font-weight:bold;
	text-align:left;
	border-bottom: 1px solid #d5d5d5;
";

?><table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">

	<tr>

		<td valign="top" width="50%">

			<h2 style="<?php echo $body_content_h2; ?>"><?php _e( 'Billing address', 'woocommerce' ); ?></h2>

			<p><?php echo $order->get_formatted_billing_address(); ?></p>

		</td>

		<?php if ( get_option( 'woocommerce_ship_to_billing_address_only' ) == 'no' && ( $shipping = $order->get_formatted_shipping_address() ) ) : ?>

		<td valign="top" width="50%">

			<h2 style="<?php echo $body_content_h2; ?>"><?php _e( 'Shipping address', 'woocommerce' ); ?></h2>

			<p><?php echo $shipping; ?></p>

		</td>

		<?php endif; ?>

	</tr>

</table>