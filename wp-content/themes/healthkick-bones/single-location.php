<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">
									<h1 class="page-title" itemprop="headline"><?php the_title();?></h1>								
								</header> <!-- end article header -->
								
								<?php 
									$meta = get_post_meta($post->ID);
									$address = isset($meta['_saal_location_address'][0]) ? $meta['_saal_location_address'][0] : null;
									$city = isset($meta['_saal_location_city'][0]) ? $meta['_saal_location_city'][0] : null;
									$state = isset($meta['_saal_location_state'][0]) ? $meta['_saal_location_state'][0] : null;
									$zip = isset($meta['_saal_location_zip'][0]) ? $meta['_saal_location_zip'][0] : null;
									$phone = isset($meta['_saal_location_phone'][0]) ? $meta['_saal_location_phone'][0] : null;
									$email = isset($meta['_saal_location_email'][0]) ? $meta['_saal_location_email'][0] : null;
									$desc = isset($meta['_saal_location_desc'][0]) ? $meta['_saal_location_desc'][0] : null;
									$showstaff = isset($meta['_saal_location_show_staff'][0]) ? $meta['_saal_location_show_staff'][0] : null;

									$addressString = 'Healthkick Nutrition Center'.' '.$address.' '.$city.' '.$state.' '.$zip;
									$addressString = urlencode($addressString);
									$gmapurl = 'https://maps.google.com/maps?f=q&hl=en&q='.$addressString.'&iwloc=near&z=16';
								?>								

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php 
									echo '<div class="location-img" style="float:right;">';
										if ( has_post_thumbnail()) {
											the_post_thumbnail('staff-thumb');
										} else {
											echo '<img src="'.get_stylesheet_directory_uri().'/library/images/location-temp-img.jpg" alt="'.get_the_title().'">';
										}
										echo '</div>';

									echo '<p>';
										if ($address) { echo $address; }
										if ($city || $state || $zip ) { 
											echo '<br>';
											if ($city) { echo $city; }
											if ($state) { echo ', '.$state; }
											if ($zip) { echo ' '.$zip; }
										}
									echo '</p>';
									if($phone || $email) {
										echo '<p>';
										if ($phone) { echo 'Phone: '.$phone;}
										if ($email) { echo '<br>Email: <a href="mailto:'.$email.'">'.$email.'</a>';
										}
										echo '</p>';
									}
									echo '<a href="'.$gmapurl.'" class="button" target="_blank">Get Directions</a>';
									
									if ($desc) {
										echo '<div class="seperator"></div>';
										echo do_shortcode(wpautop($desc));
									}?>

								</section> <!-- end article section -->

								<?php
									if ($showstaff != 2) {

									// Retrieve Staff Members 
									$staff = new WP_Query(array(
										'post_type'			=>	'staff',
										'posts_per_page'	=>	'-1',
										'orderby'			=>	'date',
										'order'				=>	'asc',
										'meta_key'			=>	'_saal_staff_location',
										'meta_value'		=> 	$post->ID
										));

									if ( $staff->have_posts() ) : 
										echo '<div class="seperator"></div>';
										echo '<h3>'.get_the_title().' Team</h3>';

										echo '<ul class="staff-list twelvecol clearfix first">';

										while ( $staff->have_posts()) : $staff->the_post();

											$meta = get_post_meta($post->ID);
											$position = isset($meta['_saal_position'][0]) ? $meta['_saal_position'][0] : null;
											$email = isset($meta['_saal_staff_email'][0]) ? $meta['_saal_staff_email'][0] : null;
											// $locationId = isset($meta['_saal_staff_location'][0]) ? $meta['_saal_staff_location'][0] : null;
											// $locationName = get_the_title($locationId);

											// $locationMeta = get_post_meta($locationId);
											// $locationPhone = isset($locationMeta['_saal_location_phone'][0]) ? $locationMeta['_saal_location_phone'][0] : null;

											echo '<li class="clearfix">';
												if ( has_post_thumbnail()) {
													the_post_thumbnail('staff-thumb');
												}

												echo '<p><span class="staff-name">'.get_the_title().'</span>';
													if ($position) { 
														echo '<span class="staff-position">'.$position.'</span>';
													}
													// if ($locationName) {
													// 	echo '<span class="staff-location"><a href="'.get_permalink($locationId).'">'.$locationName.'</a>';
													// 		if ($locationPhone) { 
													// 			echo '<br>'.$locationPhone;
													// 		}
													// 	echo '</span>';
													// }
													if ($email) {
														echo '<a href="mailto:'.$email.'" class="button small">Email</a>';
													}
												echo '</p>';
											echo '</li>';
										endwhile;

										echo '</ul>';

									endif;
								}
								?>

							</article> <!-- end article -->

							<?php endwhile; endif; ?>

						</div> <!-- end #main -->

						<?php get_sidebar(); ?>

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

<?php get_footer(); ?>
